#### **Instructions**

1: clone the repos 

`git clone https://gitlab.com/PrashanthGowda/chat-app.git`

2: open repository

`cd chat-app`

3: Install the requirements

`pip install requirements.txt`

4: create the required database

`sqlite3 chatapp.db` 

`.read SQLquery.txt` - to create necessary database

`.quit` - close the database

5: To run the server 

`python3 -m http.server`

6: To run sandman server

`sandman2ctl sqlite+pysqlite:///chatapp.db`

7: open the application in any of the browser

`localhost:8000`