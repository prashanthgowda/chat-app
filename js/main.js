function appendMessages(data) {
  let message = $("<div/>");
  let user = $("<h4>").text(data.user_id);
  let mess = $("<p>").text(data.message);
  user.appendTo(message);
  mess.appendTo(message);
  message.appendTo(".message-window");
}
function MessageScrollDown(){
  $(".message-window").animate({ scrollTop: $(".message-window div:last-child").offset().top }, 1000);
}
$(document).ready(function() {
  Pusher.logToConsole = true;
  var pusher = new Pusher("42a457224aeef2b3445e", {
    cluster: "ap2",
    forceTLS: true
  });

  var channel = pusher.subscribe("messages");
  channel.bind("message-added", function(data) {
    appendMessages(data);
    MessageScrollDown();
  });
});

// LOGIN PAGE FUNCTIONS

$(".form").ready(function() {
  $("a").click(function(event) {
    $(".register-form").toggle();
    $(".login-form").toggle();
    event.preventDefault();
  });
});
//LOGIN
function login(loginInfo) {
  $.ajax({
    type: "GET",
    url: "http://localhost:5000/users",
    data: loginInfo,
    dataType: "json",
    crossDomain: true,
    success: function(resp) {
      if (resp.resources.length == 0) {
        $("#login-display-message").text("The credentials does'nt exist");
        $("#login-display-message").addClass("login-display-message");
      } else {
        localStorage.setItem("username", $("#uid").val());
        window.location.replace("/account.html");
      }
    },
    error: function(req, status, err) {
      $("#login-display-message").text("something went wrong");
      $("#login-display-message").addClass("login-display-message");
    }
  });
}
$(".login-form").ready(function() {
  $("#login").click(function(event) {
    event.preventDefault();
    let logininfo = {
      user_id: $("#uid").val(),
      password: $("#login-pass").val()
    };
    login(logininfo);
  });
});
// REGISTRATION
function createAccount(creds) {
  $.ajax({
    type: "POST",
    url: "http://localhost:5000/users/",
    data: JSON.stringify(creds),
    dataType: "json",
    contentType: "json",
    crossDomain: true,
    success: function(resp) {
      if (resp == undefined) {
        $("#display-message").text("user name already exist");
        $("#display-message").addClass("create-acc-failure");
      } else {
        $("#display-message").text("Account Created!!!");
        $("#display-message").addClass("create-acc-success");
      }
    },
    error: function(req, status, err) {
      $("#display-message").text("something went wrong");
      $("#display-message").addClass("create-acc-failure");
    }
  });
}
function validatePass(password) {
  var number = /([0-9])/;
  var alphabets = /([a-zA-Z])/;
  var special_characters = /([~,!,@,#,$,%,^,&,*,-,_,+,=,?,>,<])/;
  if (password.length < 6) {
    $("#display-message").text("Weak (should be atleast 6 characters.)");
    $("#display-message").addClass("failure");
  } else {
    if (
      $(password)
        .val()
        .match(number) &&
      $(password)
        .val()
        .match(alphabets) &&
      $(password)
        .val()
        .match(special_characters)
    ) {
      $("#display-message").text("Strong");
      $("#display-message").addClass("success");
    } else {
      $("#display-message").text(
        "Medium (should include alphabets, numbers and special characters.)"
      );
      $("#display-message").css("color", "gold");
    }
  }
}
function confirmPassword() {
  if ($("#pass").val().length != $("#c-pass").val().length) {
    $("#display-message").text("password not a match");
    $("#display-message").addClass("failure");
  } else if ($("#pass").val() == $("#c-pass").val()) {
    $("#display-message").text("password matched");
    $("#display-message").addClass("success");
  } else {
    $("#display-message").text("password not a match");
    $("#display-message").addClass("failure")
  }
}
$(".register-form").ready(function(e) {
  $("#pass").keyup(function(e) {
    validatePass($("#pass").val());
  });

  $("#c-pass").keyup(function(e) {
    confirmPassword();
  });

  $("#create").click(function(e) {
    e.preventDefault();
    if ($("#pass").val() == $("#c-pass").val()) {
      let creds = { user_id: $("#user_id").val(), password: $("#pass").val() };
      createAccount(creds);
    } else {
      $("#display-message").text("password not a match");
      $("#display-message").addClass("failure")
    }
  });
});

//MESSAGE PAGE FUNCTIONS

$(".user-name").ready(function() {
  let name = localStorage.username;
  $(".user-name").text("Hello!!! " + name);
});
function loadChannels(data) {
  $("<div/>")
    .attr("id", data.channel_name)
    .text(data.channel_name)
    .appendTo(".channels");
}
function loadAccount() {
  $.ajax({
    type: "GET",
    url: "http://localhost:5000/channels",
    dataType: "json",
    crossDomain: true,
    success: function(resp) {
      if (resp.resources.length > 0) {
        for (let i of resp.resources) {
          loadChannels(i);
        }
        $("#" + resp.resources[0].channel_name).click();
      }
    },
    error: function(req, status, err) {
      console.log("something went wrong", status, err);
    }
  });
}
$(".wrapper").ready(function() {
  loadAccount();
});

$(".Channels-title").ready(function() {
  $("#add-ch").click(function(e) {
    e.preventDefault();
    $(".add-channels-input").toggle();
  });
});
function addChannel(channel) {
  {
    $.ajax({
      type: "POST",
      url: "http://localhost:5000/channels/",
      data: JSON.stringify(channel),
      dataType: "json",
      contentType: "json",
      crossDomain: true,
      success: function(resp) {
        if (resp == undefined) {
          $("#add-ch-name").addClass("failure");
        } else {
          $("<div/>")
            .attr("id", resp.channel_name)
            .text(resp.channel_name)
            .appendTo(".channels");
          $("#add-ch-name").addClass("success");
        }
      },
      error: function(req, status, err) {
        $("#add-ch-name").addClass("failure")
      }
    });
  }
}
$(".add-channels-input").ready(function() {
  $("#add-new-ch").click(function(e) {
    e.preventDefault();
    let channel = { channel_name: $("#add-ch-name").val() };
    if (
      $("#add-ch-name")
        .val()
        .replace(/\s/g, "").length > 0
    ) {
      addChannel(channel);
    }
  });
});
// LOAD MESSAGES
function loadMessages(cid) {
  $.ajax({
    type: "GET",
    url: "http://localhost:5000/messages/?group_id=" + cid,
    dataType: "json",
    crossDomain: true,
    success: function(resp) {
      if (resp.resources.length > 0) {
        for (let i of resp.resources) {
          appendMessages(i);
        }
        MessageScrollDown();
      }
    },
    error: function(req, status, err) {
      console.log("something went wrong", status, err);
    }
  });
}
$("#channels").ready(function() {
  $("#channels").click(function(e) {
    e.preventDefault();
    $("div").removeClass("channels-active");
    $(e.target).addClass("channels-active");
    $(".channel-name").text($(".channels-active").attr("id"));
    $(".message-window").empty();
    let cid = $(".channels-active").attr("id");
    loadMessages(cid);
  });
});
function broadcastMessages(data) {
  $.ajax({
    type: "POST",
    url: "http://localhost:5000/broadcast",
    data: JSON.stringify(data),
    dataType: "json",
    contentType: "application/json",
    crossDomain: true
  });
}
function sendMessage(data) {
  $.ajax({
    type: "POST",
    url: "http://localhost:5000/messages/",
    data: JSON.stringify(data),
    dataType: "json",
    contentType: "json",
    crossDomain: true,
    success: function(resp) {
      broadcastMessages(data);
    },
    error: function(req, status, err) {
      console.log("something went wrong", status, err);
    }
  });
}

function getUserInput() {
  let data = {
    user_id: localStorage.username,
    group_id: $(".channels-active").attr("id"),
    message: $(".user-message").val()
  };
  return data;
}

$(".message-tab").ready(function() {
  $(".user-message").keyup(function(e) {
    if (e.keyCode == 13) {
      if (
        $(".user-message")
          .val()
          .replace(/\s/g, "").length > 0
      ) {
        let data = getUserInput();
        sendMessage(data);
      }
      $(".user-message")
        .focus()
        .val("");
    }
  });
  $(".send-message").click(function(e) {
    if (
      $(".user-message")
        .val()
        .replace(/\s/g, "").length > 0
    ) {
      let data = getUserInput();
      sendMessage(data);
    }
    $(".user-message")
      .focus()
      .val("");
  });
});
